import subprocess
import time

import pywinauto
from pywinauto import application, findwindows

def main():
    print("Starting Notepad...")
    notepad_process = subprocess.Popen(['notepad.exe'])

    time.sleep(5)  # Wait for Notepad to start

    print("Logging all visible windows...")
    windows = findwindows.find_elements()
    for win in windows:
        print(f"Title: {win.name}")

    print("Connecting to Notepad...")
    app = application.Application().connect(process=notepad_process.pid, timeout=20)

    print("Looking for Notepad window...")
    dlg = app.window(title_re='.*Notepad')
    dlg.wait('visible', timeout=20)

    print("Typing text into Notepad...")
    dlg.Edit.type_keys("Hello World")

    dlg.menu_select("File->Exit")
    print("Finished!")

if __name__ == "__main__":
    main()
